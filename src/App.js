import Board from './components/Board';
import './App.css';
import { SocketProvider } from './libs/socket';
// add comment
function App() {
  return (
    <SocketProvider>
      <div className="App">
        <Board />
      </div>
    </SocketProvider>
  );
}

export default App;
