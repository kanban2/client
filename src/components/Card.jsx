import React from "react";
import { Draggable } from "react-beautiful-dnd";

const Card = ({ task, index }) => {
  return (
    <Draggable key={task.id} draggableId={`${task.id}`} index={index}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={{
            backgroundColor: "#ccc",
            padding: "12px 18px",
            borderRadius: "8px",
            ...provided.draggableProps.style,
          }}
        >
          <h4 style={{ fontFamily: "Arial", margin: "4px 0px" }}>
            {task?.title}
          </h4>
          <p style={{ fontFamily: "Arial", margin: "4px 0px" }}>
            {task?.description}
          </p>
        </div>
      )}
    </Draggable>
  );
};

export default Card;
