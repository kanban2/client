import React, { useEffect, useState } from "react";
import { Droppable } from "react-beautiful-dnd";
import { api } from "../libs/api";
import { useSocket } from "../libs/socket";
import Card from "./Card";

const Column = ({ status }) => {
  const { socket } = useSocket();
  const [tasks, setTasks] = useState([]);

  const getTasks = async () => {
    const response = await api.get(`/tasks/filter?status=${status}`);
    setTasks(response.data);
  };

  useEffect(() => {
    getTasks();
  }, [status]);

  useEffect(() => {
    if (socket) {
      socket.on("task_created", (message) => {
        getTasks();
      });
      socket.on("task_updated", (message) => {
        getTasks();
      });
    }
  }, [socket]);

  return (
    <Droppable droppableId={status}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          style={{
            backgroundColor: "#eeeeee",
            padding: "16px",
            borderRadius: "8px",
            minWidth: "300px",
            maxWidth: "300px",
            flex: "1",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <h3
            style={{
              fontFamily: "Arial",
              margin: "4px 0",
              marginBottom: "12px",
              textAlign: "center",
            }}
          >
            {status}
          </h3>
          <div
            className=""
            style={{
              flex: "1",
              display: "flex",
              flexDirection: "column",
              gap: "12px",
              overflowY: "scroll",
            }}
          >
            {tasks.map((task, task_index) => (
              <Card task={task} index={task_index} />
            ))}
          </div>
        </div>
      )}
    </Droppable>
  );
};

export default Column;
