import React, { useEffect, useState } from "react";
import { useSocket } from "../libs/socket";
import Column from "./Column";
import AddForm from "../components/AddForm";
import { DragDropContext } from "react-beautiful-dnd";
import { api } from "../libs/api";

const Board = () => {
  const [showForm, setShowForm] = useState(false);
  const { socket } = useSocket();

  useEffect(() => {
    if (socket) {
      socket.on("info", (message) => {
        console.log(message);
      });
    }
  }, [socket]);

  const onDrageEnd = async (props) => {
    const { source, destination, draggableId } = props;

    // dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      // TODO: create reordering logic
      console.log("reordering logic is not implemented");
    } else {
      const response = await api.post(`/tasks/${draggableId}`, {
        status: destination.droppableId,
      });

      console.log(response.data);
    }
  };

  return (
    <>
      <DragDropContext onDragEnd={onDrageEnd}>
        <div
          className=""
          style={{
            display: "flex",
            flexDirection: "column",
            height: "100vh",
            backgroundColor: "#666666",
          }}
        >
          <button
            style={{
              padding: "4px 12px",
              whiteSpace: "nowrap",
              width: "min-content",
            }}
            onClick={setShowForm.bind(this, !showForm)}
          >
            Add
          </button>
          <div
            style={{
              flex: "1",
              display: "flex",
              gap: "8px",
              overflowX: "auto",
              width: "100%",
              padding: "12px",
              boxSizing: "border-box",
            }}
          >
            <Column status="to_do" />
            <Column status="in_progress" />
            <Column status="ready_to_check" />
            <Column status="done" />
          </div>
        </div>
      </DragDropContext>
      {showForm && <AddForm closeFunction={setShowForm.bind(this, false)} />}
    </>
  );
};

export default Board;
