import React, { useState } from "react";
import { api } from "../libs/api";

const AddForm = ({ closeFunction }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const onAddTask = async () => {
    if (title == "") {
      console.log("title cannot be empty");
    }

    if (description == "") {
      console.log("description cannot be empty");
    }

    try {
      await api.post("/tasks", { title, description, status: "to_do" });

      setTitle("");
      setDescription("");

      closeFunction();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div
      style={{
        position: "absolute",
        width: "100vw",
        height: "100vh",
        backgroundColor: "rgba(0,0,0,0.7)",
        top: 0,
        left: 0,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          width: "50%",
          height: "50%",
          backgroundColor: "#ffffff",
          borderRadius: "12px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          gap: "16px",
        }}
      >
        <h2 style={{ fontFamily: "Arial", margin: "0" }}>Add New Task</h2>
        <input
          type="text"
          placeholder="Title"
          style={{
            width: "60%",
            backgroundColor: "#cccccc",
            borderColor: "transparent",
            padding: "8px 24px",
            borderRadius: "4px",
            textAlign: "center",
            fontSize: "16px",
          }}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <input
          type="text"
          placeholder="Description"
          style={{
            width: "60%",
            backgroundColor: "#cccccc",
            borderColor: "transparent",
            padding: "8px 24px",
            borderRadius: "4px",
            textAlign: "center",
            fontSize: "16px",
          }}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <button
          style={{
            width: "30%",
            backgroundColor: "#cccccc",
            borderColor: "transparent",
            padding: "8px 24px",
            borderRadius: "4px",
            textAlign: "center",
            fontSize: "16px",
            cursor: "pointer",
          }}
          onClick={onAddTask}
        >
          Submit
        </button>
        <button
          style={{
            width: "30%",
            backgroundColor: "#cccccc",
            borderColor: "transparent",
            padding: "8px 24px",
            borderRadius: "4px",
            textAlign: "center",
            fontSize: "16px",
            cursor: "pointer",
          }}
          onClick={closeFunction}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default AddForm;
