import axios from 'axios';

export const api = axios.create({ baseURL: 'https://kanban-server-rizaldi.herokuapp.com/api/v1' });
