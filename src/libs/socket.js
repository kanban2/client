import React, { useState, useContext, createContext, useEffect } from 'react'
import io from 'socket.io-client';

const socketContext = createContext()

export function SocketProvider({ children }) {
  const socket = useProvideSocket()
  return <socketContext.Provider value={socket}>{children}</socketContext.Provider>
}

export const useSocket = () => {
  return useContext(socketContext)
}

function useProvideSocket() {
  const [socket, setSocket] = useState(null)

  useEffect(() => {
    const temp_socket = io.connect('https://kanban-server-rizaldi.herokuapp.com');
    setSocket(temp_socket);
  }, [])

  return {
    socket,
  }
}